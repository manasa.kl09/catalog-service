package com.manasa.catalogservice.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CommonBeanUtil implements ApplicationContextAware {
	
	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;

	}
	
	public static RestTemplate getCommonRestTemplate() {
		return context.getBean(Constants.SECURED_REST_TEMPLATE, RestTemplate.class);
	}

}
