package com.manasa.catalogservice.config;

public interface Constants {
	String TLS_PROTOCOL = "TLSv1";
	public static final String TLS_PROTOCOL_TLSv1DOT2 = "TLSv1.2";
	public static final String SECURED_REST_TEMPLATE = "securedRestTemplate";
}
