package com.manasa.catalogservice.config;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;



import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Configuration
public class CataLogServiceConfig {

	@Value("${trust.store}")
	private String trustStore;

	@Value("${trust.store.password}")
	private String trustStorePassword;

	@Bean(name = Constants.SECURED_REST_TEMPLATE)
	public RestTemplate getSecuredRestTemplate() {

		log.info("Start of getSecuredRestTemplate()");
		File file = new File(getTrustStore());

		RestTemplate restTemplate = null;
		SSLContext sslcontext = null;
		try {
			sslcontext = SSLContexts.custom()
					.loadTrustMaterial(file, getTrustStorePassword().toCharArray(), new TrustSelfSignedStrategy())
					.build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | CertificateException
				| IOException e) {
			log.error("Failed to get restTemplate with truststore : {}", e);
		}
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
				new String[] { Constants.TLS_PROTOCOL, Constants.TLS_PROTOCOL_TLSv1DOT2 }, null,
				SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setSSLSocketFactory(sslsf);
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
				builder.build());
		restTemplate = new RestTemplate(clientHttpRequestFactory);
		log.info("End of getSecuredRestTemplate()");
		return restTemplate;

	}

}
