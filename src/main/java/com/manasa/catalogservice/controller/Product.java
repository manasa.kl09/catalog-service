package com.manasa.catalogservice.controller;

import lombok.Data;

@Data
public class Product {
	private String productCode;
	private int availableQuantity;
}
