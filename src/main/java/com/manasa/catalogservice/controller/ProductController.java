package com.manasa.catalogservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manasa.catalogservice.exception.ProductNotFoundException;
import com.manasa.catalogservice.repository.ProductEntity;
import com.manasa.catalogservice.service.ProductServiceImpl;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	 
    @Autowired
    private ProductServiceImpl productService;
 
    @GetMapping
    public List<ProductEntity> allProducts() {
        return productService.findAllProducts();
    }
 
    @GetMapping("/{code}")
    public ProductEntity productByCode(@PathVariable String code) {
        return productService.findProductByCode(code)
                .orElseThrow(() -> new ProductNotFoundException("Product with code ["+code+"] doesn't exist"));
    }
}
