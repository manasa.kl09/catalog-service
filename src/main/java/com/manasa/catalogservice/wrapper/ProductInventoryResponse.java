package com.manasa.catalogservice.wrapper;

import lombok.Data;

@Data
public class ProductInventoryResponse {
	private String productCode;
	private int availableQuantity;
}
