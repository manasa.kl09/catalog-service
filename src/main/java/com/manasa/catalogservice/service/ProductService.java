package com.manasa.catalogservice.service;

import java.util.List;
import java.util.Optional;

import com.manasa.catalogservice.repository.ProductEntity;


public interface ProductService {

	public List<ProductEntity> findAllProducts();
	public Optional<ProductEntity> findProductByCode(String code);
}
