package com.manasa.catalogservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EurekaClientService {

	@Autowired
	private EurekaClient eurekaClient;

	public String getServiceUrl(String serviceName) {
		String uri = "";
		Application application = eurekaClient.getApplication(serviceName);
		if (application == null) {
			log.error("Dependendent service [" + serviceName + "] is down");
		} else {
			InstanceInfo instanceInfo = application.getInstances().get(0);
			
			application.getInstances().forEach(i -> {
				log.info("HostName : " + i.getHostName());
				log.info("HomePageUrl : " + i.getHomePageUrl());
				log.info("Port : " + i.getPort());
				log.info("Id : " + i.getId());
				log.info("**************");
			});

			uri = "https://localhost:" + instanceInfo.getPort();
		}
		log.info("URI : " + uri);
		return uri;
	}
}
