package com.manasa.catalogservice.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.manasa.catalogservice.config.CommonBeanUtil;
import com.manasa.catalogservice.repository.ProductEntity;
import com.manasa.catalogservice.repository.ProductRepository;
import com.manasa.catalogservice.wrapper.ProductInventoryResponse;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private EurekaClientService eurekaClientService;

	public List<ProductEntity> findAllProducts() {
		return productRepository.findAll();
	}

	@HystrixCommand(fallbackMethod = "getDefaultProductInventoryByCode")
	public Optional<ProductEntity> findProductByCode(String code) {
		Optional<ProductEntity> productOptional = productRepository.findByCode(code);
		if (productOptional.isPresent()) {
			log.info("Fetching inventory level for product_code: " + code);
			ResponseEntity<ProductInventoryResponse> itemResponseEntity = CommonBeanUtil.getCommonRestTemplate()
					.getForEntity(eurekaClientService.getServiceUrl("inventory-service")
							+ "/inventory/filter?productCode=" + code, ProductInventoryResponse.class);
			if (itemResponseEntity.getStatusCode() == HttpStatus.OK) {
				Integer quantity = itemResponseEntity.getBody().getAvailableQuantity();
				log.info("Available quantity: " + quantity);
				productOptional.get().setInStock(quantity > 0);
			} else {
				log.error("Unable to get inventory level for product_code: " + code + ", StatusCode: "
						+ itemResponseEntity.getStatusCode());
			}
		}
		return productOptional;
	}

	@SuppressWarnings("unused")
	Optional<ProductEntity> getDefaultProductInventoryByCode(String productCode) {
		log.info("Returning default ProductInventoryByCode for productCode: " + productCode);
		ProductEntity response = new ProductEntity();
		response.setCode(productCode);
		response.setInStock(false);
		return Optional.ofNullable(response);
	}
}
